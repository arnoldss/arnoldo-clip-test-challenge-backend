import express from 'express';
import bodyParser from 'body-parser';
import charges from './routes/charges';
import customers from './routes/customers';
import payments from './routes/payments';

const app = express();
const port = 3000;

app.listen(port, () => {
  console.log(`Timezones by location application is running on port ${port}.`);
});
app.use(bodyParser.json());


app.get('/', function (req, res) {
   res.send('Hello World' + JSON.stringify(customers));
});


app.use('/charges', charges);

app.use('/customers', customers);

app.use('/payments', payments);






