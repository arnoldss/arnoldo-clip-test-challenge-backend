import express, { Router, Request, Response } from "express";
import openpay from "../openpay";

const router: Router = express.Router();
router.post('/', async (req: Request, res: Response) => {
    try {
        let charge = req.body;
        openpay.charges.create(charge, function (error, body) {
            if (error !== null) { throw error };   
            res.send(body);    
        });


    } catch (e) {
        res.status(500).send(e.toString());
    }
});

export default router;