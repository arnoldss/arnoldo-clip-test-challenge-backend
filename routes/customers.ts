import express, { Router, Request, Response } from "express";
import openpay from "../openpay";

const router: Router = express.Router();
router.post('/', async (req: Request, res: Response) => {
    try {
        let customer =  req.body;
        console.log(customer)
        openpay.customers.create(customer, function(error, body) {
            if (error !== null) { throw error };    
            res.send(body);     
        });
    } catch (e) {
        res.status(500).send(e.toString());
    }
});

router.get('/:id', async (req: Request, res: Response) => {
    try {
        let customerId =  JSON.stringify(req.params.id);
        openpay.customers.get(customerId, function(error, body) {
            if (error !== null) { throw error };    
            res.send(body);     
        });
    } catch (e) {
        res.status(500).send(e.toString());
    }
});


router.get('/', async (req: Request, res: Response) => {
    try {
        openpay.customers.list({}, function(error, body) {
            if (error !== null) { throw error };    
            res.send(body);     
        });
    } catch (e) {
        res.status(500).send(e.toString());
    }
});


router.put('/', async (req: Request, res: Response) => {
    try {
        let customerId = req.body[0].id;
        let customer = req.body[1].customer;
        openpay.groups.customers.update(customerId, customer, function(error, body) {
            if (error !== null) { throw error };    
            res.send(body);     
        });
    } catch (e) {
        res.status(500).send(e.toString());
    }
});


export default router;