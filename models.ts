export interface Customer {
     name : String ,
     email : String,
     last_name : String ,
     address :{
       city : String ,
       state : String ,
       line1 : String,
       line2 : String ,
       postal_code : String ,
       country_code : String 
    },
     phone_number : String
}

export interface Charge  {
      method :  String ,
      card : {
        card_number :  Number ,
        holder_name :  String ,
        expiration_year :  Number ,
        expiration_month :  Number ,
        cvv2 :  Number ,
     },
      amount  : Number,
      description  :  String ,
      order_id  :  String 
   };


   export interface Payout {
      method :  String ,
      bank_account :{
        clabe : Number ,
        holder_name :  String
     },
      amount  : Number,
      description  :  String
   };