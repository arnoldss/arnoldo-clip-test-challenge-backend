"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const charges_1 = __importDefault(require("./routes/charges"));
const customers_1 = __importDefault(require("./routes/customers"));
const payments_1 = __importDefault(require("./routes/payments"));
const app = (0, express_1.default)();
const port = 3000;
app.listen(port, () => {
    console.log(`Timezones by location application is running on port ${port}.`);
});
app.use(body_parser_1.default.json());
app.get('/', function (req, res) {
    res.send('Hello World' + JSON.stringify(customers_1.default));
});
app.use('/charges', charges_1.default);
app.use('/customers', customers_1.default);
app.use('/payments', payments_1.default);
//# sourceMappingURL=app.js.map